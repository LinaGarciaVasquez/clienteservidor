//Lina Marcela Garcia Vasquez
// Servidor banco

#include <czmq.h>
#include <iostream>
#include <unordered_map>
#include <string.h>
#include <cassert>

using namespace std;

typedef unordered_map<int,int> BancType;

// Server's state
BancType banco;
int cuenta = 20000;

void dispatch(zmsg_t *incmsg, zmsg_t *outmsg) {
  
  zmsg_print(incmsg);
  char *operacion = zmsg_popstr(incmsg);
  
  if(strcmp(operacion,"Crear") == 0) {
     cuenta++;
     banco[cuenta] = 0; 
     string actual = to_string(cuenta);     
     zmsg_addstr(outmsg, "Numero de cuenta: "); 
     zmsg_addstr(outmsg, actual.c_str());
     
  } else if (strcmp(operacion,"Depositar") == 0) {
     char *cuentaDep = zmsg_popstr(incmsg);
     char *saldoDep = zmsg_popstr(incmsg);
	 
	 int cuentad = atoi(cuentaDep);	
	 int saldod = atoi(saldoDep);	

	 if (banco.count(cuentad) > 0){
	   banco[cuentad] += saldod;
	   zmsg_addstr(outmsg, "Deposito exitoso!!");
	   free(cuentaDep);
           free(saldoDep);
     } else {
       zmsg_addstr(outmsg, "La cuenta no existe!!");
     }
       
  } else if (strcmp(operacion,"Retirar") == 0) {
	
     char *cuentaRet = zmsg_popstr(incmsg);
     char *saldoRet = zmsg_popstr(incmsg);
	 
	 int cuentar = atoi(cuentaRet);	
	 int saldor = atoi(saldoRet);	

	 if ((banco.count(cuentar) > 0) && (saldor < banco[cuentar])){
	   banco[cuentar] -= saldor;
	   zmsg_addstr(outmsg, "Retiro exitoso!!");
           free(cuentaRet);
           free(saldoRet);
     } else {
       zmsg_addstr(outmsg, "La cuenta no existe o el saldo es insuficiente!!");
     }
   
   } else if (strcmp(operacion,"Consultar") == 0) {

	char *cuentaCon = zmsg_popstr(incmsg);	
	
	int cuentac = atoi(cuentaCon);	
	
	if (banco.count(cuentac) > 0){
	   string resultado = to_string(banco[cuentac]); 
           zmsg_addstr(outmsg, "Su saldo es: ");
	   zmsg_addstr(outmsg, resultado.c_str()); 
	   free(cuentaCon);
     } else {
       zmsg_addstr(outmsg, "La cuenta no existe!!");
     }
	
  } else if (strcmp(operacion,"Transferencia") == 0) {

	char *cuentaTns = zmsg_popstr(incmsg);
        char *saldoTns = zmsg_popstr(incmsg);
	char *cuentaTns2 = zmsg_popstr(incmsg);

	int cuenta1 = atoi(cuentaTns);	
	int saldoT = atoi(saldoTns);
	int cuenta2 = atoi(cuentaTns2);	
	
        if (((banco.count(cuenta1) > 0) && (banco.count(cuenta2) > 0)) && (saldoT < banco[cuenta1]) ){
	   banco[cuenta2] += saldoT;
	   banco[cuenta1] -= saldoT;
	   zmsg_addstr(outmsg, "Tansferencia exitosa!!!");
	   free(cuentaTns);
	   free(saldoTns);
	   free(cuentaTns2);
        } else {
          zmsg_addstr(outmsg, "La cuenta no existe o el saldo es insuficiente!!");
        }

  } else {
    zmsg_addstr(outmsg, "Error, reintente de nuevo!!");
  }
    free(operacion);
}


int main (void)
{
  zctx_t *context = zctx_new();
  void *responder = zsocket_new(context, ZMQ_REP);
  zsocket_bind(responder, "tcp://*:5555");
  
  while (1) {
    zmsg_t* request = zmsg_recv(responder);
    zmsg_t *response = zmsg_new();
    dispatch(request,response);
    zmsg_send(&response,responder);
    }
    
    zctx_destroy(&context);
    return 0;
}
