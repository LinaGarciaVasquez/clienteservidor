//Lina Marcela Garcia Vasquez
//  Cliente Banco 
#include <czmq.h>
#include <string.h>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
 
  zctx_t *context = zctx_new();
  void *requester = zsocket_new(context, ZMQ_REQ);
  zsocket_connect(requester, "tcp://localhost:5555");

  // Creacion del mensaje
  zmsg_t* request = zmsg_new();
  
  if (strcmp(argv[1],"crear") == 0) { //Crear cuenta
    zmsg_addstr(request,"Crear");     //Formato -> Crear

  } else if (strcmp(argv[1],"depositar") == 0) {  //Depositar en la cuenta
    zmsg_addstr(request,"Depositar");             //Formato -> Depositar | # cuenta | $
    zmsg_addstr(request,argv[2]);
    zmsg_addstr(request,argv[3]);
  
  } else if (strcmp(argv[1],"retirar") == 0) {   //Retirar de la cuenta
    zmsg_addstr(request,"Retirar");              //Formato -> Retirar | # cuenta | $
    zmsg_addstr(request,argv[2]);
    zmsg_addstr(request,argv[3]);

  } else if (strcmp(argv[1],"consultar") == 0) {  //Consultar saldo
    zmsg_addstr(request,"Consultar");		  //Formato -> Consultar | # cuenta
    zmsg_addstr(request,argv[2]);
  
  } else if (strcmp(argv[1],"transferencia") == 0) { //Transferencia de la cuenta 1 a la cuenta 2
    zmsg_addstr(request,"Transferencia");	     //Formato -> Transferencia | # cuenta 1 | $ | # cuenta 2
    zmsg_addstr(request,argv[2]);
    zmsg_addstr(request,argv[3]);
    zmsg_addstr(request,argv[4]);

  } else {
    cout << "Opcion no valida!\n";
  } 
  
  // Sends message request through socket requester
  zmsg_send(&request,requester);
  
  zmsg_t* resp = zmsg_recv(requester);
  
  zmsg_print(resp);      
         
  zctx_destroy(&context);
 
  return 0;
}







